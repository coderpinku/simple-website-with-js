// model page section

 var openmodel = document.getElementById('login-modal');
 var closeBtn = document.getElementsByClassName('close-btn')[0];
 
 closeBtn.addEventListener('click', closeLogin);
 window.addEventListener('click', clickoutside);
 
 function openLoginPage(){
   openmodel.style.display = 'block';
  }
  
  function closeLogin(){
    openmodel.style.display = 'none';
    
  }
  function clickoutside(e){
    if(e.target == openmodel){
      openmodel.style.display = 'none';
    }
    
  }
  
  
  // slider section 
  
    // Get the slider element
    const slider = document.getElementById('userSlider');

    // Get the plan elements
    const plan1 = document.getElementById('plan1');
    const plan2 = document.getElementById('plan2');
    const plan3 = document.getElementById('plan3');

    // Add event listener for slider value changes
    slider.addEventListener('input', function() {
      const value = parseInt(slider.value);

      // Remove the highlighted class from all plans
      plan1.classList.remove('highlighted');
      plan2.classList.remove('highlighted');
      plan3.classList.remove('highlighted');

      // Highlight the appropriate plan based on the number of users
      if (value >= 0 && value < 10) {
        plan1.classList.add('highlighted');
      } else if (value >= 10 && value < 20) {
        plan2.classList.add('highlighted');
      } else if (value >= 20 && value <= 30) {
        plan3.classList.add('highlighted');
      }
    });

    
    
   